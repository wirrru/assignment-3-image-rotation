#include "transformations.h"
#include <math.h>
#include <stdlib.h>


#define PI 3.14159265358979323846

double find_rotated_width(uint64_t width, uint64_t height, int angle);
double find_rotated_height(uint64_t width, uint64_t height, int angle);

struct image rotate(const struct image *source, int angle) {
    struct image rotated_image;
    double rotated_width = find_rotated_width(source->width, source->height, angle);
    double rotated_height = find_rotated_height(source->width, source->height, angle);
    uint64_t rot_x, rot_y;
    init_image(&rotated_image, (uint64_t)rotated_width, (uint64_t)rotated_height);
    for (uint64_t orig_y = 0; orig_y < source->height; orig_y++) {
        for (uint64_t orig_x = 0; orig_x < source->width; orig_x++) {
            if (angle == 0) {
                rot_x = orig_x;
                rot_y = orig_y;
            } else if (angle == 90 || angle == -270) {
                rot_x = orig_y;
                rot_y = source->width - orig_x - 1;
            } else if (angle == 180 || angle == -180) {
                rot_x = source->width - orig_x - 1;
                rot_y = source->height - orig_y - 1;
            } else {
                rot_x = source->height - orig_y - 1;
                rot_y = orig_x;
            }
            if (rot_x < rotated_image.width && rot_y < rotated_image.height) {
                rotated_image.data[rot_y * rotated_image.width + rot_x] = source->data[orig_y * source->width + orig_x];
            }
        }
    }
    return rotated_image;
}

double find_rotated_width(uint64_t width, uint64_t height, int angle) {
    double radians = abs(angle) * PI / 180.0;
    return fabs((double)height * sin(radians)) + fabs((double)width * cos(radians));
}

double find_rotated_height(uint64_t width, uint64_t height, int angle) {
    double radians = abs(angle) * PI / 180.0;
    return fabs((double)height * cos(radians)) + fabs((double)width * sin(radians));
}
