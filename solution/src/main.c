#include "image.h"
#include "bmp.h"
#include "transformations.h"
#include <stdio.h>
#include <stdlib.h>


int main(__attribute__((unused)) int argc, char* argv[]) {
    FILE* source_file = fopen(argv[1], "rb");
    if (source_file == NULL) {
        fprintf(stderr, "Error opening source file\n");
        return EXIT_FAILURE;
    }
    struct image source_image;
    if (from_bmp(source_file, &source_image) != READ_OK) {
        fprintf(stderr, "Error with source_image");
        fclose(source_file);
        return EXIT_FAILURE;
    }
    fclose(source_file);
    char *angle_str = argv[3];
    char *endptr;
    long angle = strtol(angle_str, &endptr, 10);
    struct image transformed_image = rotate(&source_image, (int)angle);
    FILE* transformed_file = fopen(argv[2], "wb");
    if (to_bmp(transformed_file, &transformed_image) != WRITE_OK) {
        fprintf(stderr, "Error with transformed_image");
        fclose(transformed_file);
        de_init_image(&source_image);
        de_init_image(&transformed_image);
        return EXIT_FAILURE;
    }
    fclose(transformed_file);
    de_init_image(&source_image);
    de_init_image(&transformed_image);
    printf("Ok");
    return EXIT_SUCCESS;
}
