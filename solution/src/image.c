#include "image.h"
#include <stdio.h>
#include <stdlib.h>

void init_image(struct image* img, uint64_t width, uint64_t height) {
    img->width = width;
    img->height = height;
    img->data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
    if (img->data == NULL) {
        fprintf(stderr, "Error allocating memory for image data\n");
    }
}

void de_init_image(struct image* img) {
    if (img != NULL) {
        free(img->data);
    }
    else {
        fprintf(stderr, "Error img is NULL\n");
    }
}
