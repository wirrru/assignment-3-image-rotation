#define BMP_HEADER_SIZE 54
#define BMP_SIGNATURE 0x4D42

#include "bmp.h"
#include <stdint.h>
#include <string.h>

void write_bmp_header(FILE *out, uint64_t width, uint64_t height);
void write_padding(FILE *out, uint64_t width);

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

enum read_status from_bmp(FILE *in, struct image *img) {
    uint16_t sign;
    if (fread(&sign, sizeof(uint16_t), 1, in) != 1 || sign != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    fseek(in, 18, SEEK_SET);
    uint32_t width, height;
    if (fread(&width, sizeof(uint32_t), 1, in) != 1 ||
        fread(&height, sizeof(uint32_t), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    fseek(in, BMP_HEADER_SIZE, SEEK_SET);
    init_image(img, width, height);
    size_t row_size = (width * sizeof(struct pixel) + 3) / 4 * 4;
    for (uint64_t i = 0; i < height; ++i) {
        if (fread(img->data + i * width, sizeof(struct pixel), width, in) != width) {
            return READ_INVALID_DATA;
        }
        fseek(in, (long)(row_size - width * sizeof(struct pixel)), SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    if (img == NULL || img->data == NULL) {
        return WRITE_ERROR;
    }
    write_bmp_header(out, img->width, img->height);
    for (uint64_t i = 0; i < img->height; ++i) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        write_padding(out, img->width);
    }
    return WRITE_OK;
}

void write_bmp_header(FILE *out, uint64_t width, uint64_t height) {
    uint16_t signature = BMP_SIGNATURE;
    fwrite(&signature, sizeof(uint16_t), 1, out);

    uint32_t file_size = BMP_HEADER_SIZE + width * height * sizeof(struct pixel);
    fwrite(&file_size, sizeof(uint32_t), 1, out);

    uint32_t reserved = 0;
    fwrite(&reserved, sizeof(uint32_t), 1, out);

    uint32_t data_offset = BMP_HEADER_SIZE;
    fwrite(&data_offset, sizeof(uint32_t), 1, out);

    uint32_t header_size = 40;
    fwrite(&header_size, sizeof(uint32_t), 1, out);

    fwrite(&width, sizeof(uint32_t), 1, out);
    fwrite(&height, sizeof(uint32_t), 1, out);

    uint16_t planes = 1;
    fwrite(&planes, sizeof(uint16_t), 1, out);

    uint16_t bit_count = 24;
    fwrite(&bit_count, sizeof(uint16_t), 1, out);

    uint32_t compression = 0;
    fwrite(&compression, sizeof(uint32_t), 1, out);

    uint32_t image_size = 0;
    fwrite(&image_size, sizeof(uint32_t), 1, out);

    uint32_t x_pixels_per_meter = 0;
    fwrite(&x_pixels_per_meter, sizeof(uint32_t), 1, out);

    uint32_t y_pixels_per_meter = 0;
    fwrite(&y_pixels_per_meter, sizeof(uint32_t), 1, out);

    uint32_t clr_used = 0;
    fwrite(&clr_used, sizeof(uint32_t), 1, out);

    uint32_t clr_important = 0;
    fwrite(&clr_important, sizeof(uint32_t), 1, out);
}

void write_padding(FILE *out, uint64_t width) {
    size_t padding_size = (4 - (width * sizeof(struct pixel)) % 4) % 4;
    uint8_t padding[4] = {0};
    fwrite(padding, sizeof(uint8_t), padding_size, out);
}
